extends CharacterBody2D
var screen_size:Vector2
@onready var animated_sprite_2d: AnimatedSprite2D = $AnimatedSprite2D
@onready var shoot_timer: Timer = $ShootTimer
@onready var attribute_component: AttributeComponent = %AttributeComponent
@onready var effect_component: EffectComponent = %EffectComponent

var can_shoot:bool
signal shoot
signal health_changed(current_value)
func _ready() -> void:
	screen_size=get_viewport_rect().size
	_reset()
	attribute_component.modify_attribute.connect(on_modify_attribute)
func _reset()->void:
	position=screen_size/2	
	can_shoot=true
	effect_component.end_all_effect()
	attribute_component._reset_attributes()
func get_current_health()->float:
	return attribute_component.get_current_value(AttributeTypes.AttributeType.Health)
func _physics_process(_delta: float) -> void:
	get_input()
	move_and_slide()
	
	position=position.clamp(Vector2.ZERO,screen_size)
	var mouse=get_local_mouse_position()
	var angle=snappedf(mouse.angle(),PI/4)/(PI/4)
	angle=wrapi(int(angle),0,8)
	animated_sprite_2d.animation="Walk"+str(angle)
	if velocity.length()!=0:
		animated_sprite_2d.play()
	else:
		animated_sprite_2d.stop()
	pass
func get_input() -> void:
	var input_dir=Input.get_vector("Left","Right","Up","Down")
	velocity=input_dir.normalized()*attribute_component.move_speed.current_value
	
	if Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT) and can_shoot:
		var dir=get_global_mouse_position()-position
		shoot.emit(position,dir)
		can_shoot=false  
		shoot_timer.start()

func _on_shoot_timer_timeout() -> void:
	can_shoot=true
	pass # Replace with function body.
func on_modify_attribute(attribute:AttributeResource,modify_value:float):
	if attribute.attribute_type==AttributeTypes.AttributeType.Shoot_Speed:
		shoot_timer.wait_time=0.5/attribute.current_value
	elif attribute.attribute_type==AttributeTypes.AttributeType.Health:
		health_changed.emit(attribute.current_value)
	pass
	#if(attribute.attribute_name=="shoo")
