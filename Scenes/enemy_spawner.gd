extends Node2D
signal hit_p
signal on_enemy_death(remain_enmey_num)

@onready var main_scene=get_node("/root/Main")
@onready var timer: Timer = $Timer
@onready var markers: Node2D = $Markers
@onready var enemies: Node2D = $Enemies

var enemy_scene=preload("res://Scenes/enemy.tscn")
var spawn_points=[]
var remaining_spawn_num:int=0
var remain_enemies:int=0


func _ready() -> void:
	for i in markers.get_children():
		if i is Marker2D:
			spawn_points.append(i)
	pass
func _start_spawn_enemy(spawn_num:int,spawn_interval:float):
	remaining_spawn_num=spawn_num
	timer.wait_time=spawn_interval
	remain_enemies=spawn_num
	timer.start()
func _on_timer_timeout() -> void:
	if remaining_spawn_num>0:
		var spawn_point=spawn_points[randi()%spawn_points.size()]
		var cur_enemy=enemy_scene.instantiate()
		cur_enemy.position=spawn_point.position
		cur_enemy.hit_player.connect(hit)
		cur_enemy.owner=self
		enemies.add_child(cur_enemy)
		cur_enemy.on_death.connect(enmey_death)
		cur_enemy.add_to_group("enemies")
		remaining_spawn_num-=1
	else:
		timer.stop()
	pass # Replace with function body.
func enmey_death():
	remain_enemies-=1
	on_enemy_death.emit(remain_enemies)

func hit():
	hit_p.emit()
