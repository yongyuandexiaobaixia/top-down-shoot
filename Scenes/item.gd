extends Area2D
@onready var sprite_2d: Sprite2D = $Sprite2D

var item_effects:Array[EffectResource]=[]

var item_name:String=""
func init_by_resource(in_item:ItemResource) -> void:
	sprite_2d= $Sprite2D
	sprite_2d.texture=in_item.item_icon
	item_effects=in_item.item_effects
	item_name=in_item.item_name
	
func _on_body_entered(_body: Node2D) -> void:
	var effect_comp:EffectComponent=_body.get_node("%EffectComponent")
	if effect_comp:
		effect_comp.deal_effects(item_effects)

	queue_free()
	pass # Replace with function body.
