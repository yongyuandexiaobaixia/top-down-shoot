extends Node2D
@onready var hud: CanvasLayer = $HUD
@onready var player: CharacterBody2D = $Player
@onready var game_over: CanvasLayer = $GameOver
@onready var restart_timer: Timer = $RestartTimer
@onready var enemy_spawner: Node2D = $EnemySpawner

@export var max_enemies:int
var difficulty:float
const DIFF_MULTIPLIER:float =1.2
var enemy_spawn_interval:float=1.0

var wave:int
func _ready() -> void:
	difficulty=10
	new_game()
	game_over.get_node("GameStartButton").pressed.connect(new_game)
func new_game()->void:
	wave=1
	enemy_spawn_interval=1.0
	max_enemies=10
	get_tree().call_group("enemies","queue_free")
	get_tree().call_group("bullets","queue_free")
	get_tree().call_group("items","queue_free")
	player._reset()
	game_over.hide()
	enemy_spawner._start_spawn_enemy(max_enemies,1.0)
	update_hud_label()
	get_tree().paused=true
	restart_timer.start()

func _on_enemy_spawner_hit_p() -> void:
	var temp_modify_data= AttributeModifyData.new(AttributeTypes.AttributeType.Health,AttributeModifyData.AttributeModifyType.Modify,-1)
	EffectTypes._apply_modify_data(player.attribute_component,temp_modify_data)
	pass # Replace with function body.
func update_hud_label():
	hud.lives_label.text="X "+str(int(player.get_current_health()))
	hud.wave_label.text="WAVE: "+str(wave)
	hud.enemies_label.text=" X "+str(enemy_spawner.remain_enemies)+"  "
func _on_player_health_changed(current_value:float) -> void:
	update_hud_label()
	if current_value<=0:
		game_over.show()
		get_tree().paused=true
		game_over.get_node("WaveSurvivedLabel").text="Wave Survived: "+str(wave)
		
	pass # Replace with function body.


func _on_restart_timer_timeout() -> void:
	get_tree().paused=false
	pass # Replace with function body.


func _on_enemy_spawner_on_enemy_death(remain_enemy_num:int) -> void:
	if remain_enemy_num<=0:
		wave+=1
		difficulty*=DIFF_MULTIPLIER
		max_enemies=int(difficulty)
		if enemy_spawn_interval>0.25:
			enemy_spawn_interval-=0.05
		enemy_spawner._start_spawn_enemy(max_enemies,enemy_spawn_interval)
	update_hud_label()
	pass # Replace with function body.
