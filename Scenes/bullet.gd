extends Area2D
var speed :int=500
var direction:Vector2
const HIT_OBJECT = preload("res://Scenes/hit_object.tscn")
@onready var main: Node2D = get_node("/root/Main")
func _process(delta: float) -> void:
	position+=speed*direction*delta
func _on_timer_timeout():
	queue_free()


func _on_body_entered(body: Node2D) -> void:
	if body.name=="World":
		hit_object()
		queue_free()
	elif body.is_in_group("enemies"):
		body.die()
		hit_object()	
		queue_free()	
	pass # Replace with function body.
func hit_object():
	var hit_object=HIT_OBJECT.instantiate()
	hit_object.position=position
	hit_object.direction=direction
	main.add_child(hit_object)
	hit_object.add_to_group("hit_objects")
	hit_object.process_mode=Node.PROCESS_MODE_ALWAYS
