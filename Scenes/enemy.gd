extends CharacterBody2D
@onready var player: CharacterBody2D = get_node("/root/Main/Player")
@onready var main: Node2D = get_node("/root/Main")
@onready var animated_sprite_2d: AnimatedSprite2D = $AnimatedSprite2D
const EXPLOSION = preload("res://Scenes/explosion.tscn")


signal hit_player 
signal on_death

@export var drop_items_on_die:Array[ItemResource]=[]
var alive:bool

var entered:bool
var speed:int=100
var direction:Vector2
const DROP_CHANCE:float =0.1

func _ready() -> void:
	var screen_rect=get_viewport_rect()
	alive=true
	entered=false
	var dist=screen_rect.get_center()-position
	if abs(dist.x)>abs(dist.y):
		direction.x=dist.x
		direction.y=0
	else:
		direction.x=0
		direction.y=dist.y
func _physics_process(_delta: float) -> void:
	if alive:
		animated_sprite_2d.animation="Run"
		if entered:
			direction=(player.position-position)
		
		direction=direction.normalized()
		velocity=direction*speed
		move_and_slide()
		if velocity.x!=0:
			animated_sprite_2d.flip_h=velocity.x<0
	else:
		pass
func die():
	if alive:
		alive=false
		animated_sprite_2d.animation="Die"
		animated_sprite_2d.play()
		
		animated_sprite_2d.animation_finished.connect(die_effect)
func die_effect():
	if randf()<DROP_CHANCE:
		drop_items()
	var explosion=EXPLOSION.instantiate()
	explosion.position=position
	main.add_child(explosion)
	explosion.add_to_group("explosions")
	explosion.process_mode=Node.PROCESS_MODE_ALWAYS
	on_death.emit()
	queue_free()
func drop_items():
	var random_index:int=randi()%(drop_items_on_die.size())
	if random_index<drop_items_on_die.size():
		var temp_item_resource:ItemResource=drop_items_on_die[random_index]
		var temp_item_scene=temp_item_resource.item_scene.instantiate()
		main.call_deferred("add_child",temp_item_scene)
		temp_item_scene.add_to_group("items")
		temp_item_scene.init_by_resource(temp_item_resource)
		temp_item_scene.position=position
	
func _on_timer_timeout() -> void:
	entered=true
	pass # Replace with function body.


func _on_area_2d_body_entered(body: Node2D) -> void:
	if(body==player):
		hit_player.emit()

	pass # Replace with function body.
