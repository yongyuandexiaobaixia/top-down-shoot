extends Object
class_name EffectTypes

enum EffectExcuteType{OneShot,Duration}
enum EffectExistType{Permanent,Temporary}
enum EffectDurationType{OnlyTrigger,IntervalTrigger}

static func _apply_modify_datas(attribute_comp:AttributeComponent,modify_datas:Array[AttributeModifyData]):
	for modify_data in modify_datas:
		#设置属性
		if modify_data.modify_type==AttributeModifyData.AttributeModifyType.Modify:
			attribute_comp.modify_attribute_value(modify_data.attribute_type,modify_data.value)
		elif modify_data.modify_type==AttributeModifyData.AttributeModifyType.Set:
			attribute_comp.set_attribute_value_by_name(modify_data.attribute_type,modify_data.value)
static func _apply_modify_data(attribute_comp:AttributeComponent,modify_data:AttributeModifyData):
		#设置属性
		if modify_data.modify_type==AttributeModifyData.AttributeModifyType.Modify:
			attribute_comp.modify_attribute_value(modify_data.attribute_type,modify_data.value)
		elif modify_data.modify_type==AttributeModifyData.AttributeModifyType.Set:
			attribute_comp.set_attribute_value_by_name(modify_data.attribute_type,modify_data.value)
