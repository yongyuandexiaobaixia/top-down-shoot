class_name EffectResource extends Resource
@export var EffectScene:PackedScene=preload("res://Scripts/Effect/Effect_Base.tscn")

@export var effect_id=0
@export var ExcuteType:EffectTypes.EffectExcuteType=EffectTypes.EffectExcuteType.OneShot
@export var ExistType:EffectTypes.EffectExistType=EffectTypes.EffectExistType.Temporary
@export var modify_attributes:Array[AttributeModifyData]=[]
@export var DurationType:EffectTypes.EffectDurationType=EffectTypes.EffectDurationType.OnlyTrigger
@export var duration:float=1
@export var interval:float=1



