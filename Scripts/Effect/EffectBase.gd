class_name EffectBase extends Node2D
@onready var duration_timer: Timer = $DurationTimer
@onready var interval_timer: Timer = $IntervalTimer
@export var duration:float=1
@export var interval:float=1
@export var ExistType:EffectTypes.EffectExistType=EffectTypes.EffectExistType.Temporary
@export var DurationType:EffectTypes.EffectDurationType=EffectTypes.EffectDurationType.OnlyTrigger
@export var ExcuteType:EffectTypes.EffectExcuteType=EffectTypes.EffectExcuteType.OneShot
@export var modify_attributes:Array[AttributeModifyData]=[]
var effect_id=0
var effect_target:Node2D
var target_attribute_comp: AttributeComponent=null
var restore_modify_datas:Array[AttributeModifyData]=[]
signal effect_end(effect_scene)

func _init_by_target_resource(target:Node2D=null,in_effect:EffectResource=null)->void:
	effect_target=target
	target_attribute_comp= effect_target.get_node("%AttributeComponent")
	if(target_attribute_comp):
		_init_by_resource(in_effect)
		init_duration_type()
		_effect_start()
	else:
		push_error("target_attribute_comp is null! "+"effect_target is %s"%(effect_target.name))
		_effect_end()
func _init_by_resource(in_effect:EffectResource)->void:
	effect_id=in_effect.effect_id
	duration=in_effect.duration
	interval=in_effect.interval
	ExistType=in_effect.ExistType
	DurationType=in_effect.DurationType
	ExcuteType=in_effect.ExcuteType
	modify_attributes=in_effect.modify_attributes
	
func init_duration_type():
	duration_timer.wait_time=duration
	duration_timer.start()
	match DurationType:
		EffectTypes.EffectDurationType.IntervalTrigger:
			interval_timer.wait_time=interval
			interval_timer.start()
			pass
func _effect_start()->void:
	_execute_effect(true)
	pass

func _effect_end()->void:
	if(ExistType==EffectTypes.EffectExistType.Temporary):
		_restore_effect()
	effect_end.emit(self)
	queue_free()
	pass
func _execute_effect(b_new_restore_data:bool)->void:
	for modify_attribtue in modify_attributes:
		#更新恢复数据，恢复属性时使用
		if(ExistType==EffectTypes.EffectExistType.Temporary and b_new_restore_data ):
			_update_restore_attribute(modify_attribtue)
	EffectTypes._apply_modify_datas(target_attribute_comp,modify_attributes)		
func _update_restore_attribute(modify_attribtue:AttributeModifyData):
		restore_modify_datas.clear()
		var temp_attribute:AttributeResource=target_attribute_comp.get_attribute(modify_attribtue.attribute_type);
		if(null!=temp_attribute):
			var temp_modify_data=AttributeModifyData.new(temp_attribute.attribute_type,AttributeModifyData.AttributeModifyType.Set,temp_attribute.current_value)
			restore_modify_datas.append(temp_modify_data)
func _restore_effect():
	for modify_attribtue in restore_modify_datas:
		target_attribute_comp.set_attribute_value_by_name(modify_attribtue.attribute_type,modify_attribtue.value)			

func _on_duration_timer_timeout() -> void:
	_effect_end()
	pass # Replace with function body.
func _set_duration(new_value:float)->void:
	duration_timer.stop()
	duration_timer.wait_time=new_value
	duration_timer.start()

func _on_interval_timer_timeout() -> void:
	_execute_effect(false)
	pass # Replace with function body.
