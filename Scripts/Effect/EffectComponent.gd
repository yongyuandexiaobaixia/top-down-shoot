class_name EffectComponent extends Node
var attribute_comp:AttributeComponent
var current_effect_scenes:Array[Node]=[]
func _ready() -> void:
	attribute_comp=owner.get_node("%AttributeComponent")
	
func deal_effects(effects:Array[EffectResource])->void:
	for effect in effects:
		if effect.ExcuteType==EffectTypes.EffectExcuteType.OneShot:
			EffectTypes._apply_modify_datas(attribute_comp,effect.modify_attributes)
		elif effect.ExcuteType==EffectTypes.EffectExcuteType.Duration:
			var current_scene:Node=get_current_scene_by_effect(effect)
			if current_scene:
				scene_existent(current_scene,effect)
				print("existent")
			else:
				scene_non_existent(effect)
				print("non_existent")
			
func scene_existent(current_scene:Node,effect:EffectResource):
	#重新开始计时
	current_scene._set_duration(effect.duration)
	pass
func scene_non_existent(effect:EffectResource):
	var effect_scene=effect.EffectScene.instantiate()
	add_child(effect_scene)
	effect_scene._init_by_target_resource(owner,effect)
	effect_scene.add_to_group("effects")
	current_effect_scenes.append( effect_scene)
	pass
func end_all_effect():
	for current_effect_scene in current_effect_scenes:
		if current_effect_scene!=null:
			current_effect_scene._effect_end()
	current_effect_scenes.clear()
				
	
	
func get_current_scene_by_effect(in_effect:EffectResource)->Node:
	var target_effect_scene=null
	for index in range(current_effect_scenes.size()-1,-1,-1):
		var current_effect_scene=current_effect_scenes[index]
		if !current_effect_scene:
			current_effect_scenes.remove_at(index)
			continue
		if(current_effect_scene.effect_id== in_effect.effect_id):
			target_effect_scene=current_effect_scene
	return target_effect_scene
