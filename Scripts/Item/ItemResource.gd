class_name ItemResource extends Resource
const ItemBaseSence=preload("res://Scenes/item.tscn")
@export var item_scene:PackedScene=ItemBaseSence
@export var item_name:String=""
@export var item_icon:Texture2D=null
@export var item_effects:Array[EffectResource]=[]
func _init(in_item_scene:PackedScene=ItemBaseSence,in_item_name:String="",in_item_icon:Texture2D=null,in_item_effects:Array[EffectResource]=[]) -> void:
	item_scene=in_item_scene
	item_name=in_item_name
	item_effects=in_item_effects
	item_icon=in_item_icon
