class_name AttributeComponent extends Node
signal modify_attribute(attribute,modify_value)

@export var attribute_table:AttributeTable=null
var move_speed:AttributeResource
var shoot_speed:AttributeResource
var health:AttributeResource

func _ready() -> void:
	move_speed=AttributeResource.new()
	shoot_speed=AttributeResource.new()
	health=AttributeResource.new()
	_init_by_table(attribute_table)
func _reset_attributes()->void:
	_init_by_table(attribute_table)

func _init_by_table(in_table:AttributeTable)->void:
	for attribute in in_table.attributes:
		var temp_attribute=type_to_resource(attribute.attribute_type)
		temp_attribute._init_by_attribute_resource(attribute)

func type_to_resource(in_type:AttributeTypes.AttributeType)->AttributeResource:
	match in_type:
		AttributeTypes.AttributeType.Health:
			return health
		AttributeTypes.AttributeType.Shoot_Speed:
			return shoot_speed
		AttributeTypes.AttributeType.Move_Speed:
			return move_speed
	return null

func get_attribute(in_type:AttributeTypes.AttributeType)->AttributeResource:
	var temp_attribute:AttributeResource= AttributeResource.new()
	temp_attribute._init_by_attribute_resource(type_to_resource(in_type))
	return temp_attribute
func get_current_value(in_type:AttributeTypes.AttributeType)->float:
	var attribute= type_to_resource(in_type)
	if(attribute):
		return attribute.current_value
	return 0.0
func modify_attribute_value(in_type:AttributeTypes.AttributeType,modify_value:float)->void:
	var attribute= type_to_resource(in_type)
	if(attribute):
		set_attribute_value(attribute,attribute.current_value+modify_value)
func set_attribute_value_by_name(in_type:AttributeTypes.AttributeType,target_value:float)->void:	
	var attribute= type_to_resource(in_type)
	if(attribute):
		set_attribute_value(attribute,target_value)
func set_attribute_value(attribute:AttributeResource,target_value:float)->void:
	attribute.current_value=clampf(target_value,attribute.min_value,attribute.max_value)
	modify_attribute.emit(attribute,target_value-attribute.current_value)
	

