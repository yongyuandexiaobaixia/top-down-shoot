class_name AttributeModifyData extends Resource
enum AttributeModifyType{Modify,Set}
@export_category("Attribute")
@export var attribute_type:AttributeTypes.AttributeType=AttributeTypes.AttributeType.Health
@export var modify_type:AttributeModifyType=AttributeModifyType.Modify
@export var value:float=0.0
func _init(in_attribute_type:AttributeTypes.AttributeType=AttributeTypes.AttributeType.Health,in_modify_type:AttributeModifyType=AttributeModifyType.Modify,in_value:float=0)->void:
	attribute_type=in_attribute_type
	modify_type=in_modify_type
	value=in_value	
