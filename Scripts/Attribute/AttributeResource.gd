class_name AttributeResource extends Resource

@export_category("Attribute")
@export var attribute_type:AttributeTypes.AttributeType=AttributeTypes.AttributeType.Health
@export var current_value:float=0.0
@export var max_value:float=0.0
@export var min_value:float=0.0
func _init(in_attribute_type: AttributeTypes.AttributeType=AttributeTypes.AttributeType.Health, in_min_value: float = 0.0, in_max_value: float = 0.0, in_current_value: float = 0.0) -> void:
	attribute_type = in_attribute_type
	min_value = in_min_value
	max_value = in_max_value
	current_value = in_current_value
	
func _init_by_attribute_resource(in_attribute_resource:AttributeResource) -> void:
	attribute_type = in_attribute_resource.attribute_type
	min_value = in_attribute_resource.min_value
	max_value = in_attribute_resource.max_value
	current_value = in_attribute_resource.current_value
